package TestTrie.index;

import TestTrie.reader.InputReader;
import TestTrie.tree.Trie;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNull;

public class WordIndexerTest {

    private static final String NAMES_FILE_PATH = "pathForRealFile";
    private static final String EMPTY_FILE_PATH = "pathForEmptyFile";
    private static final String WRONG_FILE_PATH = "pathForWrongFile";

    private static final String EXISTING_SEARCH_WORD = "existingSearchWord";
    private static final String NOT_EXISTING_SEARCH_WORD = "notExistingSearchWord";

    @Mock
    private InputReader inputReader;
    @Mock
    private Trie trie;
    private WordIndexer sut;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        mockInputReader();
        mockTrie();
        sut = new WordIndexer(trie, inputReader);
    }

    @Test
    public void shouldCallInputReader() {
        sut.loadFile(NAMES_FILE_PATH);
        verify(inputReader).getLines(NAMES_FILE_PATH);
    }

    @Test
    public void shouldPutAllNames_FromInputReader() {
        sut.loadFile(NAMES_FILE_PATH);
        verify(trie, times(getNamesLines().size())).put(anyString(), anyInt());
    }

    @Test
    public void shouldPutOnlyNamesAndItPositions() {
        List<String> names = getNamesLines();
        sut.loadFile(NAMES_FILE_PATH);
        for (int i = 0; i < names.size(); ++i) {
            verify(trie, times(1)).put(names.get(i), i);
        }
    }

    @Test
    void shouldNotPutAnyString_IfEmptyFile() {
        withEmptyFile();
        sut.loadFile(EMPTY_FILE_PATH);
        verify(trie, never()).put(anyString(), anyInt());
    }

    @Test
    public void shouldNotPutAnyString_IfFileNotFound() {
        withWrongFile();
        sut.loadFile(WRONG_FILE_PATH);
        verify(trie, never()).put(anyString(), anyInt());
    }

    @Test
    public void shouldReturnSetOfPositions_IfKeyIsContained() {
        assertEquals(getExistingIndexes(), sut.getIndexes4Word(EXISTING_SEARCH_WORD));
    }

    @Test
    public void shouldReturnNull_IfKeyIsNotContained() {
        assertNull(sut.getIndexes4Word(NOT_EXISTING_SEARCH_WORD));
    }

    private void mockInputReader() {
        when(inputReader.getLines(NAMES_FILE_PATH)).thenReturn(getNamesLines());
    }

    private void mockTrie() {
        when(trie.find(EXISTING_SEARCH_WORD)).thenReturn(getExistingIndexes());
        when(trie.find(NOT_EXISTING_SEARCH_WORD)).thenReturn(null);
    }

    private void withEmptyFile() {
        when(inputReader.getLines(EMPTY_FILE_PATH)).thenReturn(Collections.emptyList());
    }

    private void withWrongFile() {
        when(inputReader.getLines(WRONG_FILE_PATH)).thenReturn(Collections.emptyList());
    }

    private List<String> getNamesLines() {
        return asList(
                "снаряга",
                "шуба",
                "стул",
                "стул",
                "черт",
                "кочерга",
                "телефон",
                "стол",
                "стул",
                "портмоне",
                "стул"
        );
    }

    private Set<Integer> getExistingIndexes() {
        Set<Integer> indexes = new TreeSet<>();
        indexes.addAll(asList(1, 5, 8, 9));
        return indexes;
    }
}