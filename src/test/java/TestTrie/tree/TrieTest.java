package TestTrie.tree;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashSet;

import static java.util.Arrays.asList;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class TrieTest {

    private static final String KEY_1 = "key1";
    private static final String KEY_2 = "key2";
    private static final String KEY_3 = "key3";
    private static final String KEY_1_WITH_CASE = "Key1";
    private static final int INDEX_1 = 1;
    private static final int INDEX_2 = 2;
    private static final int INDEX_3 = 3;

    private Trie sut;

    @BeforeMethod
    public void setUp() {
        sut = new Trie();
    }

    @Test
    public void shouldPutKeyWithIndex() {
        sut.put(KEY_1, INDEX_1);
        assertEquals(sut.find(KEY_1), new HashSet<>(asList(INDEX_1)));
    }

    @Test
    public void shouldPutKeysWithIndexes() {
        sut.put(KEY_1, INDEX_1);
        sut.put(KEY_2, INDEX_2);
        sut.put(KEY_3, INDEX_3);
        assertEquals(sut.find(KEY_1), new HashSet<>(asList(INDEX_1)));
        assertEquals(sut.find(KEY_2), new HashSet<>(asList(INDEX_2)));
        assertEquals(sut.find(KEY_3), new HashSet<>(asList(INDEX_3)));
    }

    @Test
    public void shouldPutDifferentIndexes_OnSameKey() {
        sut.put(KEY_1, INDEX_1);
        sut.put(KEY_1, INDEX_2);
        sut.put(KEY_1, INDEX_3);
        assertEquals(sut.find(KEY_1), new HashSet<>(asList(INDEX_1, INDEX_2, INDEX_3)));
    }

    @Test
    public void shouldReturnNull_IfTryingToFindWrongKey() {
        sut.put(KEY_2, INDEX_2);
        assertNull(sut.find(KEY_1));
    }

    @Test
    public void shouldNotIgnoreCase() {
        sut.put(KEY_1, INDEX_1);
        assertNull(sut.find(KEY_1_WITH_CASE));
    }
}