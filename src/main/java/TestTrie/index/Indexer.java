package TestTrie.index;

import java.util.Set;

public interface Indexer {

    void loadFile(String fileName);

    Set<Integer> getIndexes4Word(String searchWord);
}
