package TestTrie.index;

import TestTrie.reader.BufferedInputReader;
import TestTrie.reader.InputReader;
import TestTrie.tree.Tree;
import TestTrie.tree.Trie;

import java.util.List;
import java.util.Set;

public class WordIndexer implements Indexer {

    private Tree trie;
    private InputReader reader;

    //Конструктор только для тестирования в main
    public WordIndexer() {
        trie = new Trie();
        reader = new BufferedInputReader();
    }

    public WordIndexer(Tree trie, InputReader reader) {
        this.trie = trie;
        this.reader = reader;
    }

    @Override
    public void loadFile(String fileName) {
        List<String> keys = reader.getLines(fileName);
        for (int i = 0; i < keys.size(); ++i) {
            trie.put(keys.get(i), i);
        }
    }

    @Override
    public Set<Integer> getIndexes4Word(String searchWord) {
        return trie.find(searchWord);
    }
}
