package TestTrie.reader;

import org.slf4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

public class BufferedInputReader implements InputReader {

    private static final Logger LOGGER = getLogger(BufferedInputReader.class);
    private static final String FILE_NOT_FOUND = "Файл не найден по указанному пути: %s";
    private static final String IO_EXCEPTION = "Ошибка ввода/вывода для файла: %s";

    @Override
    public List<String> getLines(String fileName) {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            List<String> lines = new ArrayList<>();
            while (bufferedReader.ready()) {
                //Валидации нет, ибо нет инфы как валидировать, как реагировать если не пройдет
                lines.add(bufferedReader.readLine());
            }
            return lines;
        } catch (FileNotFoundException e) {
            LOGGER.error(String.format(FILE_NOT_FOUND, fileName), e);
        } catch (IOException e) {
            LOGGER.error(String.format(IO_EXCEPTION, fileName), e);
        }

        return Collections.emptyList();
    }
}
