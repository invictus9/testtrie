package TestTrie.reader;

import java.util.List;

public interface InputReader {

    List<String> getLines(String fileName);
}
