package TestTrie.tree;

import TestTrie.tree.node.TrieNode;

import java.util.Set;

public class Trie implements Tree{

    private TrieNode root = new TrieNode();

    public void put(String key, int position) {
        TrieNode tmp = root;
        for (char ch : key.toCharArray()) {
            if (!tmp.getChildren().containsKey(ch)) {
                tmp.getChildren().put(ch, new TrieNode());
            }
            tmp = tmp.getChildren().get(ch);
        }
        tmp.getValues().add(position);
    }

    public Set<Integer> find(String key) {
        TrieNode tmp = root;
        for (char ch : key.toCharArray()) {
            if (!tmp.getChildren().containsKey(ch)) {
                return null;
            } else {
                tmp = tmp.getChildren().get(ch);
            }
        }

        return tmp.getValues().isEmpty() ? null : tmp.getValues();
    }
}
