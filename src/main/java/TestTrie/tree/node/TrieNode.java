package TestTrie.tree.node;

import java.util.*;

public class TrieNode {

    private Character element;
    private Map<Character, TrieNode> children;
    private Set<Integer> values;

    public TrieNode() {
        this.children = new HashMap<>();
        this.values = new TreeSet<>();
    }

    public TrieNode(char element, Map<Character, TrieNode> children) {
        this.element = element;
        this.children = children;
        this.values = new TreeSet<>();
    }

    public Map<Character, TrieNode> getChildren() {
        return children;
    }

    public Set<Integer> getValues() {
        return values;
    }
}
