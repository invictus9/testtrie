package TestTrie.tree;

import java.util.Set;

public interface Tree {

    void put(String key, int position);

    Set<Integer> find(String key);
}
