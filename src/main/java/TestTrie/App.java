package TestTrie;

import TestTrie.index.WordIndexer;

//main - демонстрация тестирования с реальным файлом. Весь класс App будет удален при реальном использовании модуля
//Основное тестирование в кдассе TrieTest
public class App 
{
    public static void main( String[] args ) {
        WordIndexer wordIndexer = new WordIndexer();
        wordIndexer.loadFile("src\\resources\\java\\TestTrie\\text.txt");
        System.out.println("Индексы слова \"стул\":" + wordIndexer.getIndexes4Word("стул"));
        System.out.println("Проверка регистра. Поиск \"стуЛ\": " + wordIndexer.getIndexes4Word("стуЛ"));
    }
}
