Bloody enterprise edition.

Task:
There is a large text file in which various words are stored, some of them many times.
The Word Indexer class is essentially an index. For a given word, it searches for all occurrences (positions) of it in the file.
Methods:
public void load File(String fileName) - loading data from a file and building an index.
public Set<Integer> getIndexes4Word(String searchWord) - list of the word position in the file. If there is no word in
	the file, it returns null.

---- Explanations
1. Testing is presented in Treetest.
2. Programming via interfaces. Unit test coverage.
3. This project was created as a module that will be used in other projects, gradually replenished and expanded
	by other implementations. Therefore, such a division into packages with classes and interfaces
